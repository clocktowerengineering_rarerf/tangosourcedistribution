#!/bin/bash

# nuke old javas
sudo apt remove java-common

# grab new javs
sudo apt update
sudo apt get install --yes ant automake build-essential cmake git libcos4-dev libmariadb-dev libmariadbclient-dev mariadb-server maven libboost-all-dev libomniorb4-dev libomnithread4-dev libzmq3-dev omniidl python2 python3-pip openjdk-8-jdk
sudo pip3 install -r https://raw.githubusercontent.com/tango-controls/tango-doc/9.3.4/requirements.txt
git config --global advice.detachedHead false


# get tango things built and installed
ant clean
ant build
cd build/cpp
configure --enable-mariadb --with-mysql-admin=tangouser --with-mysql-admin-passwd=tangouser --enable-static
make all
sudo make install

# push services
cd ../../assets/services
cp * /etc/systemd/system/


git clone --depth 1 --branch v1.0.1 https://github.com/tango-controls-hdbpp/libhdbpp.git
cd libhdbpp/
mkdir build
cd build/
cmake ..
export CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:/usr/local/include/tango
make
sudo make install



# push startup infos to bash
cat ../../assets/bashscript/bashrc >> ~./bashrc
